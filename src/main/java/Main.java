import pl.sda.MyDumbClass;
import pl.sda.MyPrivateDumbClass;

public class Main {
    public static void main(String[] args) {
        //1. Spraw, żeby poniższy kod wypisał hello world
        MyDumbClass myDumbObject = new MyDumbClass();
        myDumbObject.printSomeString();
        //2. Wypisz wartość pola MyDumbClass.SOME_PASSWORD
        //3. W jakiś sposób utwórz obiekt MyPrivateDumbClass i spraw, żeby metoda printSomeString wypisała hello world
        MyPrivateDumbClass myPrivateDumbObject = new MyPrivateDumbClass();
        myDumbObject.printSomeString();
        //4. Wywołaj wszystkie metody, mające w sobie słówko call, które znajdują się w klasach w pakiecie pl.sda.methods
        //5. Wywołaj wszystkie metody, które są oznaczone adnotacją @Action w pakiecie pl.sda.classes
    }
}
